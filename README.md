
**RUNNING**
  
with npm  
- npm install  
- npm start  
  
with yarn  
- yarn install  
- yarn start  
  
**NOTES**  
  
main tutorial  
- https://medium.codylamson.com/minimum-viable-webpack-babel-react-setup-without-create-react-app-47959f43fbac
  
Error: hot loader issue  
- https://teamtreehouse.com/community/reacthotloaderindexjs-is-not-a-loader-must-have-normal-or-pitch-function
  
Error: Cannot create property 'mappings' on string  
- https://stackoverflow.com/questions/42698214/webpack-2-cannot-create-property-mappings-on-string
